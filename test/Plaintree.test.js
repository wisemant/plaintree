const assert = require('assert');
const Graph = require('../Plaintree');

const plain = [
  { id: 1, parent: 0, text: 'one' },
  { id: 2, parent: 0, text: 'two' },
  { id: 3, parent: 1, text: 'three' },
  { id: 4, parent: 2, text: 'four' },
  { id: 5, parent: 1, text: 'five' },
  { id: 6, parent: 3, text: 'six' },
  { id: 7, parent: 3, text: 'seven' },
];
const tree = {
  id: 0,
  children: [
    {
      id: 1,
      text: 'one',
      parent: 0,
      children: [
        {
          id: 3,
          parent: 1,
          text: 'three',
          children: [
            {
              id: 6, parent: 3, text: 'six',
            },
            {
              id: 7, parent: 3, text: 'seven',
            },
          ],
        },
        {
          id: 5, parent: 1, text: 'five',
        },
      ],
    },
    {
      id: 2,
      parent: 0,
      text: 'two',
      children: [
        {
          id: 4, parent: 2, text: 'four',
        },
      ],
    },
  ],
};

let graph;

describe('Graph test', () => {
  it('constructor', () => {
    graph = new Graph(plain);
    assert.ok(graph instanceof Graph);
  });
  it('getTree', () => {
    const backTree = graph.getTree();
    assert.deepEqual(backTree, tree);
  });
  it('getPlain', () => {
    const backPlain = graph.getPlain();
    assert.deepEqual(plain, backPlain);
  });
  it('walk', () => {
    const count = 7;
    let counter = 0;
    const walker = (elem) => {
      counter += 1;
      assert.ok(typeof elem === 'object');
      assert.ok('id' in elem);
    };
    graph.walk(walker);
    assert.equal(count, counter);
  });
  it('find', () => {
    const condition = elem => elem.text === 'three';
    const found = graph.find(condition);
    assert.equal(found.id, 3);
  });
  it('findById', () => {
    const id = 3;
    const found = graph.findById(id);
    assert.equal(found.id, 3);
  });
  it('findAll', () => {
    const condition = elem => elem.id > 5;
    const founds = graph.findAll(condition);
    assert.equal(founds.length, 2);
    assert.equal(founds[0].text, 'six');
  });
  it('filter', () => {
    const condition = elem => elem.id < 5;
    const founds = graph.filter(condition);
    assert.equal(founds.getPlain().length, 4);
  });
  it('delete', () => {
    const node = { id: 3 };
    graph.delete(node);
    const found = graph.findById(3);
    assert.equal(found, undefined);
  });
  it('append', () => {
    const node = { id: 10 };
    const parent = graph.findById(1);
    graph.append(node, parent);
    const found = graph.findById(10);
    assert.equal(found.id, 10);
    assert.equal(found.parent, 1);
  });
});
