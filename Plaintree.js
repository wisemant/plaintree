class Plaintree {
  constructor(source, {
    id = 'id',
    parent = 'parent',
    children = 'children',
    rootId = 0,
  } = {}) {
    this.id = id;
    this.parent = parent;
    this.children = children;
    this.rootId = rootId;
    this._map = new Map();


    if (Array.isArray(source)) {
      /* check */
      if (!source.every(u => u[this.id])) throw new Error('no id in data array');
      if (!source.every(u => typeof u === 'object')) throw new Error('element is not object');

      source.forEach(u => this._map.set(u[this.id], Object.assign({}, u)));
    } else
    if (this.children in source) {
      const act = (elem) => {
        const noob = Object.assign({}, elem);
        this._map.set(elem[this.id], noob);
        if (elem[this.children]) elem[this.children].forEach(act);
      };

      source[this.children].forEach(act);
    }
  }
  tree() {
    // mappin, because each array element of data is object
    const tree = {
      [this.id]: this.rootId,
      [this.children]: [],
    };
    const map = {};
    this._map.forEach((u, i) => { map[i] = Object.assign({}, u); });

    // create a forest
    for (const key of Object.keys(map)) {
      const el = map[key];

      if (!(this.parent in el)) el[this.parent] = this.rootId;
      const parentId = el[this.parent];

      if (parentId === this.rootId) { // -> append root level element to root
        tree[this.children].push(el);
        continue;
      }

      if (!map[parentId]) continue; // no parent -> lost element

      if (!map[parentId][this.children]) map[parentId][this.children] = [];
      map[parentId][this.children].push(el);
    }

    return tree;
  }

  /* actions */
  walk(callback) {
    this._map.forEach(callback);
    return this;
  }

  find(callback) {
    for (const elem of this._map.values()) {
      if (callback(elem)) return elem;
    }
    return undefined;
  }

  findById(id) {
    return this._map.get(id);
  }

  findAll(callback) {
    const res = [];
    for (const elem of this._map.values()) {
      if (callback(elem)) res.push(elem);
    }
    return res;
  }

  filter(callback) {
    const survived = this.findAll(callback);
    const mid = new Plaintree(survived);
    return new Plaintree(mid.tree());
  }

  append(_node, _nest) {
    this._map.set(_node[this.id], Object.assign({}, _node));
    const node = this._map.get(_node[this.id]); // for further operations
    const nest = this.findById(_nest[this.id]);
    node[this.parent] = nest[this.id];
  }

  delete(node) {
    this._map.delete(node[this.id]);
  }

  getTree() {
    return this.tree();
  }
  getPlain() {
    const mid = new Plaintree(this.tree());
    const map = mid._map;
    map.forEach((u) => { delete u[this.children]; });
    return Array.from(map, x => x[1]).sort((a, b) => a[this.id] - b[this.id]);
  }
}
module.exports = Plaintree;
